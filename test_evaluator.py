import unittest
from evaluator import infix_evaluate


class TestInfixEvaluate(unittest.TestCase):
    def test_infix_evaluate(self):
        cases = [
            ("* + 5 2 3", None, 21),
            ("* 5 2", None, 10),
            ("9", None, 9),
            ("+ 6 * - 4 + 2 3 8", None, -2),
            ("+ * / 4 2", None, None),
            ("+/56", None, None),
            ("+ x y", {
                "x": (1, 3),
                "y": (5, 7)
            }, 8),
            ("+ * x 5 y", {
                "x": (1, 3),
                "y": (5, 7)
            }, 16)
        ]
        for case, variables, result in cases:
            output = infix_evaluate(case, variables)
            self.assertEqual(output, result)


if __name__ == "__main__":
    unittest.main()
