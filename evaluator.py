from itertools import product


def infix_evaluate(expression: str, variables: dict = None):
    _expr = expression.split(" ")
    operators = []
    variable_ranges = []
    variable_names = []

    for i, element in enumerate(_expr):
        if element in ["+", "-", "*", "/"]:
            operators.append(i)
        elif element.isalpha():
            variable_names.append(element)

    if not len(operators) == len(_expr) - len(operators) - 1:
        return None
    elif len(_expr) == 1 and _expr[0].isdigit():
        return int(_expr[0])
    elif len(_expr) <= 1:
        return None

    if len(variable_names) > 0 and variables:
        all_results = []

        for var in variable_names:
            lower, upper = variables[var][0], variables[var][1]
            variable_ranges.append(list(range(lower, upper)))

        combinations = list(product(*variable_ranges))

        for combo in combinations:
            d = dict(zip(variable_names, combo))
            repl_expr = [x for x in _expr]
            for i, item in enumerate(repl_expr):
                if item in d.keys():
                    repl_expr[i] = d[item]

            result = eval_expr(repl_expr, operators)

            all_results.append(result[0])

        return max(all_results)

    else:
        _expr = eval_expr(_expr, operators)

    if len(_expr) == 1:
        return int(_expr[0])
    else:
        return None


def eval_expr(expr: list, operators: list):
    for ix in operators[::-1]:
        operator = expr[ix]
        if operator == "+":
            _result = int(expr[ix + 1]) + int(expr[ix + 2])
            expr[ix] = _result
        elif operator == "-":
            _result = int(expr[ix + 1]) - int(expr[ix + 2])
            expr[ix] = _result
        elif operator == "*":
            _result = int(expr[ix + 1]) * int(expr[ix + 2])
            expr[ix] = _result
        elif operator == "/":
            _result = int(expr[ix + 1]) // int(expr[ix + 2])
            expr[ix] = _result
        del expr[ix + 2]
        del expr[ix + 1]

    return expr
