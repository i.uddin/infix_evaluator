# Infix / Polish notation evaluator
Python function (`infix_evaluate()`) to evaluate an infix or Polish notation experession. 

As opposed to ordinary prefix notation where the operator is in between the operators e.g. `5 + 6`, infix notation places the operator before the *two* following operands e.g. `+ 5 6`. The operands and operators must also be separated between each other by a single space. Nested operators may also be specified such as `* - 1 5 6` which evaluates to `(1 - 5) * 6`.

The function also supports the use of variables in place of the operands (numbers) such as `+ x y`, where these variables are specified in the second argument to the fuction as a dictionary. The dictionary specifies the upper (exclusive) and lower (inclusive) boundaries for the range of numbers the given variable may take on. For example, a dictionary may take on the form `{'x': (1, 3), 'y': (5, 7)}`, which specifies `[1,2]` as the allowed values for `x`, and `[5,6]` as the allowed values for `y`. In such a case, the function will evaluate all combinations produced through the Cartesian product of the ranges for each variable, and return the largest result found.

## Usage
```python
>>> infix_evaluate("+ * x 5 y", {
        "x": (1, 3),
        "y": (5, 7)
    })
16
```

## Tests
Test cases are provided to ensure the function evaluates the expressions appropriately, or returns a `None` if the expression is invalid. These tests can be run using the `pytest` command (requires `pytest` to be installed) or by running the `test_main.py` file.


